import ballerina/grpc;
import ballerina/io;


listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "Rep" on ep {

    remote function addNewFunction(FN value) returns responseM|error {
        responseM res = {message: "hi"};
        io:println(value);
        return res;
 }

//      remote function addNewFunction(FN value) returns responseM|error {
//         responseM res = {message: "hi"};
//         io:println(value);
//         return res;
//  }
     remote function deleteFN(string value) returns responseM|error {
        responseM  res  = {message: "Hi"};
        io:println(value);
        return res;
    
     }
     remote function showFN(showAllFnId value) returns FN|error {
        FN fn ={ data:"kddkjdjkjdds",
              metaData: {
                  Developer: {
                      emailAddress: "mereky@gmail.com",
                      fullName: "Mereky amos"
                  },keywords: ["sjksdkksd","dslds", "kjsdkjsdk"],
                  language: "JAVA"
              }  
        };
        io:println(value);
        return fn;
     }

    //  int count = 0;
    //  if count === 1{
    //      io:println("+");
    //  }
      remote function addFNS(stream<FN, grpc:Error?> clientStream) returns responseM|error {
                 responseM res = {};
                 check clientStream.forEach(function(FN fn){
                     io:println(fn);
                     res = {message:"hello"};
                 });
                 return res;
     }
     remote function ShowAllFNS(string value) returns stream<FN, error?>|error {
               FN fn ={ data:"kddkjdjkjdds",
              metaData: {
                  Developer: {
                      emailAddress: "mereky@gmail.com",
                      fullName: "Mereky amos"
                  },keywords: ["sjksdkksd","dslds", "kjsdkjsdk"],
                  language: "JAVA"
              }  
        };
        io:println(value);
        FN [] array = [fn,fn,fn];
        return array.toStream();
     }
     remote function ShowAllWithCriteria(stream<string, grpc:Error?> clientStream) returns stream<FN, error?>|error {
           FN [] functions = [];
           check clientStream.forEach(function(string names){
           FN fn ={ data:"kddkjdjkjdds",
              metaData: {
                  Developer: {
                      emailAddress: "mereky@gmail.com",
                      fullName: "Mereky amos"
                  },keywords: ["sjksdkksd","dslds", "kjsdkjsdk"],
                  language: "JAVA"
              }  
        };
        io:println(names);
        functions.push(fn);
           });
           return functions.toStream();
     }
}

