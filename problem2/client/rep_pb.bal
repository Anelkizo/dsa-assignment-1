import ballerina/grpc;

public isolated client class RepClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function addNewFunction(FN|ContextFN req) returns (responseM|grpc:Error) {
        map<string|string[]> headers = {};
        FN message;
        if (req is ContextFN) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Rep/addNewFunction", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <responseM>result;
    }

    isolated remote function addNewFunctionContext(FN|ContextFN req) returns (ContextResponseM|grpc:Error) {
        map<string|string[]> headers = {};
        FN message;
        if (req is ContextFN) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Rep/addNewFunction", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <responseM>result, headers: respHeaders};
    }

    isolated remote function deleteFN(string|ContextString req) returns (responseM|grpc:Error) {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Rep/deleteFN", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <responseM>result;
    }

    isolated remote function deleteFNContext(string|ContextString req) returns (ContextResponseM|grpc:Error) {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Rep/deleteFN", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <responseM>result, headers: respHeaders};
    }

    isolated remote function showFN(showAllFnId|ContextShowAllFnId req) returns (FN|grpc:Error) {
        map<string|string[]> headers = {};
        showAllFnId message;
        if (req is ContextShowAllFnId) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Rep/showFN", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <FN>result;
    }

    isolated remote function showFNContext(showAllFnId|ContextShowAllFnId req) returns (ContextFN|grpc:Error) {
        map<string|string[]> headers = {};
        showAllFnId message;
        if (req is ContextShowAllFnId) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Rep/showFN", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <FN>result, headers: respHeaders};
    }

    isolated remote function addFNS() returns (AddFNSStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("Rep/addFNS");
        return new AddFNSStreamingClient(sClient);
    }

    isolated remote function ShowAllFNS(string|ContextString req) returns stream<FN, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("Rep/ShowAllFNS", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        FNStream outputStream = new FNStream(result);
        return new stream<FN, grpc:Error?>(outputStream);
    }

    isolated remote function ShowAllFNSContext(string|ContextString req) returns ContextFNStream|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("Rep/ShowAllFNS", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        FNStream outputStream = new FNStream(result);
        return {content: new stream<FN, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function ShowAllWithCriteria(string|ContextString req) returns stream<FN, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("Rep/ShowAllWithCriteria", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        FNStream outputStream = new FNStream(result);
        return new stream<FN, grpc:Error?>(outputStream);
    }

    isolated remote function ShowAllWithCriteriaContext(string|ContextString req) returns ContextFNStream|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("Rep/ShowAllWithCriteria", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        FNStream outputStream = new FNStream(result);
        return {content: new stream<FN, grpc:Error?>(outputStream), headers: respHeaders};
    }
}

public client class AddFNSStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendFN(FN message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextFN(ContextFN message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveResponseM() returns responseM|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <responseM>payload;
        }
    }

    isolated remote function receiveContextResponseM() returns ContextResponseM|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <responseM>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class FNStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|FN value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|FN value;|} nextRecord = {value: <FN>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class RepFNCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFN(FN response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFN(ContextFN response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class RepResponseMCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendResponseM(responseM response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextResponseM(ContextResponseM response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextFNStream record {|
    stream<FN, error?> content;
    map<string|string[]> headers;
|};

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextResponseM record {|
    responseM content;
    map<string|string[]> headers;
|};

public type ContextFN record {|
    FN content;
    map<string|string[]> headers;
|};

public type ContextShowAllFnId record {|
    showAllFnId content;
    map<string|string[]> headers;
|};

public type metaData record {|
    string language = "";
    Developer Developer = {};
    string[] keywords = [];
|};

public type responseM record {|
    string message = "";
|};

public type FN record {|
    string data = "";
    string Id = "";
    metaData metaData = {};
    int 'version = 0;
|};

public type Developer record {|
    string fullName = "";
    string emailAddress = "";
|};

public type showAllFnId record {|
    int 'version = 0;
    string id = "";
|};

const string ROOT_DESCRIPTOR = "0A097265702E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F224B0A09446576656C6F706572121A0A0866756C6C4E616D65180120012809520866756C6C4E616D6512220A0C656D61696C41646472657373180220012809520C656D61696C41646472657373226C0A086D65746144617461121A0A086C616E677561676518012001280952086C616E677561676512280A09446576656C6F70657218022001280B320A2E446576656C6F7065725209446576656C6F706572121A0A086B6579776F72647318032003280952086B6579776F72647322690A02464E12120A0464617461180120012809520464617461120E0A0249641802200128095202496412250A086D6574614461746118032001280B32092E6D6574614461746152086D6574614461746112180A0776657273696F6E180420012805520776657273696F6E22250A09726573706F6E73654D12180A076D65737361676518012001280952076D65737361676522370A0B73686F77416C6C466E496412180A0776657273696F6E180120012805520776657273696F6E120E0A026964180220012809520269643287020A0352657012210A0E6164644E657746756E6374696F6E12032E464E1A0A2E726573706F6E73654D121B0A06616464464E5312032E464E1A0A2E726573706F6E73654D280112340A0864656C657465464E121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0A2E726573706F6E73654D121B0A0673686F77464E120C2E73686F77416C6C466E49641A032E464E12310A0A53686F77416C6C464E53121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A032E464E3001123A0A1353686F77416C6C576974684372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A032E464E3001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "rep.proto": "0A097265702E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F224B0A09446576656C6F706572121A0A0866756C6C4E616D65180120012809520866756C6C4E616D6512220A0C656D61696C41646472657373180220012809520C656D61696C41646472657373226C0A086D65746144617461121A0A086C616E677561676518012001280952086C616E677561676512280A09446576656C6F70657218022001280B320A2E446576656C6F7065725209446576656C6F706572121A0A086B6579776F72647318032003280952086B6579776F72647322690A02464E12120A0464617461180120012809520464617461120E0A0249641802200128095202496412250A086D6574614461746118032001280B32092E6D6574614461746152086D6574614461746112180A0776657273696F6E180420012805520776657273696F6E22250A09726573706F6E73654D12180A076D65737361676518012001280952076D65737361676522370A0B73686F77416C6C466E496412180A0776657273696F6E180120012805520776657273696F6E120E0A026964180220012809520269643287020A0352657012210A0E6164644E657746756E6374696F6E12032E464E1A0A2E726573706F6E73654D121B0A06616464464E5312032E464E1A0A2E726573706F6E73654D280112340A0864656C657465464E121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0A2E726573706F6E73654D121B0A0673686F77464E120C2E73686F77416C6C466E49641A032E464E12310A0A53686F77416C6C464E53121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A032E464E3001123A0A1353686F77416C6C576974684372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A032E464E3001620670726F746F33"};
}

