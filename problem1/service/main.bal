import ballerina/io;
import ballerina/http;
import ballerinax/mongodb;

mongodb:ClientConfig connection 
= {
    host:"localhost",
    port:27017,
    options: {sslEnabled:false , 
    serverSelectionTimeout: 70000}
};

mongodb:Client con = check new (connection, "StudentDatabase");

service /Student on new http:Listener(8000){
     resource function post createLearnerProfile(@http:Payload json learnerProfile ) returns json|error?{
         io:println(learnerProfile.toJsonString());
         map<json> studentprofile = <map<json>>learnerProfile;
        
        check con->insert(studentprofile, "Learner_profile");
        con->close();
        return learnerProfile;
     }

     resource function put updateLeanerProlie(@http:Payload json newLearnerProfile) returns json|error?{
         string responseMessage = "";
         map<json> updatedProflie = <map<json>>newLearnerProfile;
         map<json>|error updatedfilter = {"username":check updatedProflie.username};
         
         if(updatedfilter is error){
             io:println("Error");
         }
         else{
             int response = checkpanic con->update(updatedProflie, "learner_profile",(), updatedfilter, true);
           if(response > 0){
               io:println("Count: '" +response.toString() +"'." );
               responseMessage = "Updated";
           }
           else{
               io:println("No update was completed");
               responseMessage = "No update was completed";
           }

         }
         json response = {
             message:
            responseMessage};
         
         return response;
     }
}

