import ballerina/io;
import ballerina/http;

 http:Client clientEndpoint = check new ("http://localhost:8080"); 

public function main() returns error? {

    json studentprofile = {
        username: "anelka@gmail.com",
        lastname: "Anelka",
        firstname: "Amadila",
        preferred_formats: ["audio", "video", "text"],
                past_subjects: [
                    {
                        course: "Algorithms",
                        score: "B+"
                    },
                    {
                    course: "Programming I",
                    score: "A+"
                    }
                ]
     };

      json value = checkpanic clientEndpoint->post("/Student/createLearnerProfile", studentprofile);
      io:println(value);

        json updateStudentprofile = {
               username: "anelka@gmail.com",
        lastname: "Anelka",
        firstname: "Amadila",
        preferred_formats: ["audio", "video", "text"],
                past_subjects: [
                    {
                        course: "Algorithms",
                        score: "B+"
                    },
                    {
                    course: "Programming I",
                    score: "A+"
                    }
                ]
     };
      json updateValue = checkpanic clientEndpoint->post("/Student/updateLeanerProlie", studentprofile);
      io:println(updateValue);
}
